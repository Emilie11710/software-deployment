## Note
Un problème de réception des backquotes `` de la commande echo est à noter. 
- Il faudrait remplacer la réponse renvoyée, par cette ligne sur la route menant à /deploy :
***
    > response.write(`<li>Hello <ul> 1er argument: ${nodeArg[0]}</ul>, <ul>et voici l'argument qui contenait des espaces ${nodeArg[1]}</ul></li>`);
## Launch scripts
- To set up the environment:
***
   > ./setup.sh
- To run server with appropriate env argument
***
   > ./deploy.sh <i>first_argument<i> <i>"second argument with spaces"<i>
