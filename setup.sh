#!/bin/bash

sudo apt-get update
sudo apt-get install nodejs npm
curl https://raw.githubusercontent.com/creationix/nvm/v0.25.0/install.sh
nvm install 15.3.0

mkdir staging pre-production production
export FOLDER1=$PWD/staging
export FOLDER2=$PWD/pre-production
export FOLDER3=$PWD/production
echo "FOLDER and path added in Env variables"

npm init --yes
npm install express

touch server.js

echo "
var http = require('http');
// It retrieves the arguments passed to node process
const nodeArg = process.argv.slice(2);
var server = http.createServer(function(request response) {
	if (request.url === "/staging") {
        response.write("<h1>Hello staging</h1>");
    }
  	else if (request.url === "/pre-production") {
        response.write("<h1>Hello pre-production</h1>");
    }
	else if (request.url === "/production") {
        response.write("<h1>Hello production</h1>");
    }
    else if (request.url === "/deploy") {
        // The line below should be replaced by the one in README.txt
        response.write("<h1>Hello production</h1>");
    }
	else {
		response.write("<h1>Not found folder</h1>")
	}
    response.end();
});
server.listen(6060);" > server.js

nodejs server.js